var express = require('express');
var router = express.Router();
var UserController = require('../controllers/users.js');

//User routes
router.get('/users:count', UserController.listUsers);
router.get('/users/:id', UserController.usersReadOne);
router.put('/users/:id', UserController.usersUpdateOne);
router.delete('/users/:id', UserController.usersDeleteOne);


router.post('/upload', function(req, res){
	res.send('/upload hit');
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;