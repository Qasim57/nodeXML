var mongoose = require('mongoose');

var itemSchema = new mongoose.Schema({
	id: String,
	name: String,
	power: Number
});

var userSchema = new mongoose.Schema({
	id: String,
	name: String,
	level: Number,
	items: [itemSchema]
});

var gameSchema = new mongoose.Schema({
	users: [userSchema]
});

mongoose.model('Game', gameSchema);//NameOfModel, Schema