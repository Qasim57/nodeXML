var mongoose = require('mongoose');
require('./game');
var dbURI = 'mongodb://localhost/gamesXML';
mongoose.connect(dbURI);

mongoose.connection.on('connected', function () {
	console.log('Connected to ' + dbURI);
});

mongoose.connection.on('error', function (error) {
	console.log('Mongoose connection error ' + error);
});

mongoose.connection.on('disconnected', function () {
	console.log('Mongoose disconnected');
});

//Closing the Mongoose connection when the application stops is a best-practice, handling that here
var gracefulShutdown = function (msg, callback) {
	mongoose.connection.close(function(){
		console.log('Mongoose disconnected through ' + msg);
		callback();
	});
};

//For nodemon restarts
process.once('SIGUSR2', function () {
	gracefulShutdown('nodemon restart', function () {
		process.kill(process.pid, 'SIGUSR2');
	});
});

//Unix based app termination
process.once('SIGINT', function () {
	gracefulShutdown('App termination', function () {
		process.exit(0);
	});
});

//Heroku app termination
process.once('SIGTERM', function () {
	gracefulShutdown('Heroku app shutdown', function () {
		process.exit(0);
	});
});