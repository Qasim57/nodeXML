//Dummy controller entries. Will plug in XML instead of JSON after Mongo
module.exports.listUsers = function(req, res){
	res.send("Listing users");
}

module.exports.usersReadOne = function (req, res) {
	sendJSONResponse (res, 200, {"status" : "success"});
}

module.exports.usersUpdateOne = function (req, res) {
	sendJSONResponse (res, 200, {"status" : "success"});
}

module.exports.usersDeleteOne = function (req, res) {
	sendJSONResponse (res, 200, {"status" : "success"});
}

var sendJSONResponse = function (res, status, content) {
	res.status(status);
	res.json(content);
}